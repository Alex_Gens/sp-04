External repository
------------------

[gitlab.com](https://gitlab.com/Alex_Gens/sp-04)

System requirements
-------------------

Java Development Kit v.1.8.0_201

Maven 4.0.0

Software
-------
Java 8

Developers
---------

name: Kazakov Alexey

email: aleks25000@gmail.com

Build application
-----------------
    mvn clean

    mvn install

Run application
-------------

    mvn tomcat7:run



