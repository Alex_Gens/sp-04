package ru.kazakov.iteco;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kazakov.iteco.client.ProjectClient;
import ru.kazakov.iteco.client.TaskClient;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.model.Task;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TaskTest {

    @NotNull
    private final String testId = UUID.randomUUID().toString();

    @NotNull
    private final String testName = "Test";

    @NotNull
    private final String url = "http://localhost:8080/";

    @Nullable
    private TaskDTO testDto;

    @Before
    public void initTestDto() {
        testDto = new TaskDTO();
        testDto.setDateCreate(new Date());
        testDto.setId(testId);
        testDto.setName(testName);
        TaskClient.client(url).addTask(testDto);
    }

    @After
    public void termTestDto() {
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(testId);
        if (dto != null) {
            TaskClient.client(url).deleteTask(testId);
        }
        testDto = null;
    }

    @Test
    public void getAllTasks() {
        @NotNull final TaskDTO test2 = new TaskDTO();
        test2.setName("test2");
        test2.setId(UUID.randomUUID().toString());
        test2.setDateCreate(new Date());
        TaskClient.client(url).addTask(test2);
        @Nullable final List<TaskDTO> dtos = TaskClient.client(url).getAllTask();
        Assert.assertNotNull(dtos);
        Assert.assertEquals(2, dtos.size());
        TaskClient.client(url).deleteTask(test2.getId());
    }

    @Test
    public void getTask() {
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(testId);
        Assert.assertNotNull(dto);
        Assert.assertEquals(dto.getId(), testId);
    }

    @Test
    public void addTask() {
        @NotNull final TaskDTO test2 = new TaskDTO();
        test2.setName("test2");
        test2.setId(UUID.randomUUID().toString());
        test2.setDateCreate(new Date());
        TaskClient.client(url).addTask(test2);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(test2.getId());
        Assert.assertNotNull(dto);
        Assert.assertEquals(test2.getId(), dto.getId());
        TaskClient.client(url).deleteTask(test2.getId());
    }

    @Test
    public void deleteTask() {
        TaskClient.client(url).deleteTask(testId);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(testId);
        Assert.assertNull(dto);
    }

    @Test
    public void updateTask() {
        Assert.assertNotNull(testDto);
        @NotNull final String updatedName = "TestUpdated";
        testDto.setName(updatedName);
        TaskClient.client(url).updateTask(testDto);
        @Nullable final TaskDTO dto = TaskClient.client(url).getTask(testId);
        Assert.assertNotNull(dto);
        Assert.assertEquals(dto.getName(), updatedName);
    }

}
