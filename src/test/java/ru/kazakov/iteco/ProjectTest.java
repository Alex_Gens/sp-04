package ru.kazakov.iteco;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kazakov.iteco.client.ProjectClient;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.model.Project;

import javax.validation.constraints.Null;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ProjectTest {

    @NotNull
    private final String testId = UUID.randomUUID().toString();

    @NotNull
    private final String testName = "Test";

    @NotNull
    private final String url = "http://localhost:8080/";

    @Nullable
    private ProjectDTO testDto;

    @Before
    public void initTestDto() {
        testDto = new ProjectDTO();
        testDto.setDateCreate(new Date());
        testDto.setId(testId);
        testDto.setName(testName);
        ProjectClient.client(url).addProject(testDto);
    }

    @After
    public void termTestDto() {
        @Nullable final ProjectDTO dto = ProjectClient.client(url).getProject(testId);
        if (dto != null) {
            ProjectClient.client(url).deleteProject(testId);
        }
        testDto = null;
    }

    @Test
    public void getAllProjects() {
        @NotNull final ProjectDTO test2 = new ProjectDTO();
        test2.setName("test2");
        test2.setId(UUID.randomUUID().toString());
        test2.setDateCreate(new Date());
        ProjectClient.client(url).addProject(test2);
        @Nullable final List<ProjectDTO> dtos = ProjectClient.client(url).getAllProject();
        Assert.assertNotNull(dtos);
        Assert.assertEquals(2, dtos.size());
        ProjectClient.client(url).deleteProject(test2.getId());
    }

    @Test
    public void getProject() {
        @Nullable final ProjectDTO dto = ProjectClient.client(url).getProject(testId);
        Assert.assertNotNull(dto);
        Assert.assertEquals(dto.getId(), testId);
    }

    @Test
    public void addProject() {
        @NotNull final ProjectDTO test2 = new ProjectDTO();
        test2.setName("test2");
        test2.setId(UUID.randomUUID().toString());
        test2.setDateCreate(new Date());
        ProjectClient.client(url).addProject(test2);
        @Nullable final ProjectDTO dto = ProjectClient.client(url).getProject(test2.getId());
        Assert.assertNotNull(dto);
        Assert.assertEquals(test2.getId(), dto.getId());
        ProjectClient.client(url).deleteProject(test2.getId());
    }

    @Test
    public void deleteProject() {
        ProjectClient.client(url).deleteProject(testId);
        @Nullable final ProjectDTO dto = ProjectClient.client(url).getProject(testId);
        Assert.assertNull(dto);
    }

    @Test
    public void updateProject() {
        Assert.assertNotNull(testDto);
        @NotNull final String updatedName = "TestUpdated";
        testDto.setName(updatedName);
        ProjectClient.client(url).updateProject(testDto);
        @Nullable final ProjectDTO dto = ProjectClient.client(url).getProject(testId);
        Assert.assertNotNull(dto);
        Assert.assertEquals(dto.getName(), updatedName);
    }

}
