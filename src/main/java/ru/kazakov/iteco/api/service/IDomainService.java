package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.model.Project;
import ru.kazakov.iteco.model.Task;

public interface IDomainService {

    @Nullable
    public ProjectDTO getProjectDTO(@Nullable final Project project);

    @Nullable
    public Project getProjectFromDTO(@Nullable final ProjectDTO dto) throws Exception;

    @Nullable
    public TaskDTO getTaskDTO(@Nullable final Task task);

    @Nullable
    public Task getTaskFromDTO(@Nullable final TaskDTO dto) throws Exception;

}
