package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import ru.kazakov.iteco.model.Task;

import java.util.List;

public interface ITaskService {

    public Task save(@Nullable final Task task) throws Exception;

    public Task findById(@Nullable final String id) throws Exception;

    public boolean existsById(@Nullable final String id) throws Exception;

    public List<Task> findAll();

    public List<Task> findAll(@Nullable final Sort sort) throws Exception;

    public void deleteById(@Nullable final String id) throws Exception;

    public void delete(@Nullable final Task task) throws Exception;

    public void deleteAll();

}
