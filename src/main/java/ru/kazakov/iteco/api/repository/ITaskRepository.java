package ru.kazakov.iteco.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.kazakov.iteco.model.Task;

public interface ITaskRepository extends PagingAndSortingRepository<Task, String> {}

