package ru.kazakov.iteco.enumeration;

import org.jetbrains.annotations.NotNull;

public enum SortType {

    CREATED("Created"),
    START("Start date"),
    FINISH("Finish date"),
    STATUS("Status");

    @NotNull
    private String displayName;

    private SortType(@NotNull final String displayName) {this.displayName = displayName;}

    @NotNull
    public String getDisplayName() {return this.displayName;}

}
