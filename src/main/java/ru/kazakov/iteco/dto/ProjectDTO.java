package ru.kazakov.iteco.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.kazakov.iteco.enumeration.Status;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDTO {

    @NotNull
    private String id;

    @Nullable
    private String name;

    @NotNull
    @JsonSerialize(as = Date.class)
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Date dateCreate;

    @Nullable
    @JsonSerialize(as = Date.class)
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Date dateStart;

    @Nullable
    @JsonSerialize(as = Date.class)
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @Nullable
    private String description;

    @Override
    public String toString() {
        return name;
    }

}
