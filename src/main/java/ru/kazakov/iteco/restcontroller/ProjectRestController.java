package ru.kazakov.iteco.restcontroller;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.model.Project;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping(value = "/projects",
                produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class ProjectRestController {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @GetMapping
    public List<ProjectDTO> getAllProjects() throws Exception {
        return projectService.findAll(Sort.by("dateCreate"))
                .stream().map(v -> domainService.getProjectDTO(v)).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ProjectDTO getProject(@PathVariable @Nullable final String id) throws Exception {
        @Nullable final Project project = projectService.findById(id);
        return domainService.getProjectDTO(project);
    }

    @PostMapping(consumes =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<ProjectDTO> addProject(@RequestBody @Nullable final ProjectDTO dto) throws Exception {
        @Nullable final Project project = domainService.getProjectFromDTO(dto);
        @NotNull final Project newProject = projectService.save(project);
        @Nullable final ProjectDTO newDto = domainService.getProjectDTO(newProject);
        if (newDto == null) throw new Exception();
        @NotNull final ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentContextPath();
        @NotNull final UriComponents uriComponents = builder.path("/projects/{id}").buildAndExpand(newDto.getId());
        @NotNull final URI uri = uriComponents.toUri();
        @NotNull final ResponseEntity<ProjectDTO> responseEntity = ResponseEntity.created(uri).body(newDto);
        return responseEntity;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProject(@PathVariable @Nullable final String id) throws Exception {
        projectService.deleteById(id);
    }

    @PutMapping(consumes =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public void updateProject(@RequestBody @Nullable final ProjectDTO dto) throws Exception {
        @Nullable final Project project = domainService.getProjectFromDTO(dto);
        projectService.save(project);
    }

}
