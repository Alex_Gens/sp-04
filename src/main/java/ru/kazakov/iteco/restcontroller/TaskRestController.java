package ru.kazakov.iteco.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.model.Task;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping(value = "/tasks",
                produces =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class TaskRestController {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @GetMapping
    public List<TaskDTO> getAllTasks() throws Exception {
        return taskService.findAll(Sort.by("dateCreate"))
                .stream().map(v -> domainService.getTaskDTO(v)).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public TaskDTO getTask(@PathVariable @Nullable final String id) throws Exception {
        @Nullable final Task task = taskService.findById(id);
        return domainService.getTaskDTO(task);
    }

    @PostMapping(consumes =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<TaskDTO> createTask(@RequestBody @Nullable final TaskDTO dto) throws Exception {
        @Nullable final Task task = domainService.getTaskFromDTO(dto);
        @NotNull final Task newTask = taskService.save(task);
        @Nullable final TaskDTO newDto = domainService.getTaskDTO(task);
        if (newDto == null) throw new Exception();
        @NotNull final ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentContextPath();
        @NotNull final UriComponents uriComponents = builder.path("/tasks/{id}").buildAndExpand(newDto.getId());
        @NotNull final URI uri = uriComponents.toUri();
        @NotNull final ResponseEntity<TaskDTO> responseEntity = ResponseEntity.created(uri).body(newDto);
        return responseEntity;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTask(@PathVariable @Nullable final String id) throws Exception {
        taskService.deleteById(id);
    }

    @PutMapping(consumes =  {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public void updateTask(@RequestBody @Nullable final TaskDTO dto) throws Exception {
        @Nullable final Task task = domainService.getTaskFromDTO(dto);
        taskService.save(task);
    }

}