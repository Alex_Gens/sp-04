package ru.kazakov.iteco.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.enumeration.Status;
import ru.kazakov.iteco.model.Task;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @RequestMapping("/all_tasks")
    public String getTasks(@NotNull final Model model) throws Exception {
        List<Task> tasks = taskService.findAll(Sort.by("dateCreate"));
        List<TaskDTO> dtos = tasks.stream().map(v -> domainService.getTaskDTO(v)).collect(Collectors.toList());
        model.addAttribute("entityList", dtos);
        return "task/task";
    }

    @GetMapping("/task_create")
    public String createTask(@NotNull final Model model) throws Exception {
        @NotNull final Task task = new Task();
        @Nullable final TaskDTO dto = domainService.getTaskDTO(task);
        @Nullable final List<ProjectDTO> projects = projectService.findAll(Sort.by("dateCreate")).stream()
                .map(v -> domainService.getProjectDTO(v)).collect(Collectors.toList());
        model.addAttribute("task", dto);
        model.addAttribute("projects", projects);
        model.addAttribute("values", Status.values());
        return "task/task_create";
    }

    @PostMapping(value = "/task_create")
    public String addNewTask(
            @ModelAttribute("task") @Nullable final TaskDTO dto
    ) throws Exception {
        taskService.save(domainService.getTaskFromDTO(dto));
        return "redirect:/all_tasks";
    }

    @GetMapping(value = "/task/{id}")
    public String viewTask(
            @NotNull final Model model,
            @PathVariable final String id
    ) throws Exception {
        @Nullable final Task task = taskService.findById(id);
        if (task == null) throw new Exception();
        @Nullable final TaskDTO dto = domainService.getTaskDTO(task);
        model.addAttribute("task", dto);
        return "/task/task_view";
    }

    @GetMapping(value = "/task_update/{id}")
    public String updateTask(
            @NotNull final Model model,
            @PathVariable final String id
    ) throws Exception {
        @Nullable final Task task = taskService.findById(id);
        if (task == null) throw new Exception();
        @Nullable final TaskDTO dto = domainService.getTaskDTO(task);
        @Nullable final List<ProjectDTO> projects = projectService.findAll(Sort.by("dateCreate")).stream()
                .map(v -> domainService.getProjectDTO(v)).collect(Collectors.toList());
        model.addAttribute("task", dto);
        model.addAttribute("projects", projects);
        model.addAttribute("values", Status.values());
        return "/task/task_update";
    }

    @PostMapping(value = "/task_update/{id}")
    public String mergeTask(
            @ModelAttribute final TaskDTO dto
    ) throws Exception {
        if (dto == null) throw new Exception();
        @Nullable final Task task = domainService.getTaskFromDTO(dto);
        taskService.save(task);
        return "redirect:/all_tasks";
    }

    @GetMapping(value = "/task_remove/{id}")
    public String removeTask(
            @PathVariable final String id
    ) throws Exception {
        taskService.deleteById(id);
        return "redirect:/all_tasks";
    }

    @GetMapping("/task/redirect")
    public String redirect() {return "redirect:/all_tasks";}
    
}
