package ru.kazakov.iteco.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.kazakov.iteco.enumeration.Status;
import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tasks")
public class Task extends AbstractEntity {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @Column
    @Nullable
    private String name;

    @NotNull
    @Column(name = "date_create")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateCreate = new Date();

    @Nullable
    @Column(name = "date_start")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateStart;

    @Nullable
    @Column(name = "date_finish")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateFinish;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @Column(name = "description")
    @Nullable
    private String description;

}
