<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html
><head>
    <title>Projects</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body>
<style>
    h1 {
        border-bottom: 1px dashed black;
    }

    body {
        margin: 0;
        padding: 0;
    }

    button {
        font-size: 1.1em;
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 10px 28px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }
</style>

<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td bgcolor="black">&nbsp;</td>
        <td bgcolor="black">
            <table width="100%" cellpadding="10" cellspacing="0" border="0" style="border-collapse: collapse; color: white; " bgcolor="black">
                <tbody><tr>
                    <td nowrap="nowrap" height="50">
                        <a href="/" target="_blank" style="color: white; text-decoration: none;">MAIN</a>
                        <a style="color: white; text-decoration: none;">|</a>
                        <a href="/all_projects" style="color: white; text-decoration: none;">PROJECTS</a>
                        <a style="color: white; text-decoration: none;">|</a>
                        <a href="/all_tasks" style="color: white; text-decoration: none;">TASKS</a>
                    </td>
                </tr>
                </tbody></table>
        </td>
        <td bgcolor="black">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td width="20" nowrap="nowrap">&nbsp;</td>
        <td width="100%" height="100%" valign="top">

            <table width="100%" cellpadding="10" cellspacing="0" border="1" style="margin-bottom: 20px; border-collapse: collapse;">
                <tbody><tr>
                    <td colspan="7" align="center">
                        <b>PROJECTS</b>
                    </td>
                </tr>
                <tr>
                    <td align="center" width="50" height="50">
                        <b>&#8470;</b>
                    </td>
                    <td align="left" width="15%">
                        <b>ID</b>
                    </td>
                    <td align="left" width="15%">
                        <b>NAME</b>
                    </td>
                    <td align="left" width="50%">
                        <b>DESCRIPTION</b>
                    </td>
                    <td align="center" width="6%">
                        <b>VIEW</b>
                    </td>
                    <td align="center" width="6%">
                        <b>EDIT</b>
                    </td>
                    <td align="center" width="6%">
                        <b>REMOVE</b>
                    </td>
                </tr>
                <c:forEach var="project" items="${entityList}" varStatus="loop">
                    <tr>
                        <td align="center" width="50" height="50">
                            <b>${loop.count}.</b>
                        </td>
                        <td align="left" width="15%">
                            <b>${project.id}</b>
                        </td>
                        <td align="left" width="15%">
                            <b>${project.name}</b>
                        </td>
                        <td align="left" width="50%">
                            <b>${project.description}</b>
                        </td>
                        <td align="center" width="6%">
                            <a href="${pageContext.request.contextPath}/project/${project.id}">VIEW</a>
                        </td>
                        <td align="center" width="6%">
                            <a href="${pageContext.request.contextPath}/project_update/${project.id}">EDIT</a>
                        </td>
                        <td align="center" width="6%">
                            <a href="${pageContext.request.contextPath}/project_remove/${project.id}">REMOVE</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody></table>

            <table width="20%" cellspacing="0" cellpadding="10" border="0" style="border-collapse: collapse; margin-bottom: 20px;">
                <tbody>
                <tr>
                    <form action="${pageContext.request.contextPath}/project_create">
                    <td colspan="1" align="left">
                        <button type="submit">CREATE</button>
                    </td>
                    </form>
                    <form action="${pageContext.request.contextPath}/project/redirect">
                    <td colspan="2" align="left">
                        <button type="submit" style="background-color: #e0e0e0; color: black">REFRESH</button>
                    </td>
                    </form>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="20" nowrap="nowrap">&nbsp;</td>
    </tr>
    </tbody></table></td>
</tr>
</tbody></table>

</body></html>
