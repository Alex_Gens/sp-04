<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html
><head>
    <title>Project</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body>
<style>
    h1 {
        border-bottom: 1px dashed black;
    }

    body {
        margin: 0;
        padding: 0;
    }

    input[type="text"], input[type="date"], input[type="number"], input[type="password"] {
        border: 1px solid black;
        border-radius: 3px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        -khtml-border-radius: 3px;
        background: #ffffff !important;
        outline: none;
        height: 34px;
        width: 350px;
        font-family: 'RobotoLight', serif;
        color: black;
        font-size: 1.6em;
    }

    button {
        font-size: 1.1em;
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 10px 28px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
    }
</style>

<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td bgcolor="black">&nbsp;</td>
        <td bgcolor="black">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; color: white; " bgcolor="black">
                <tbody><tr>
                    <td nowrap="nowrap" height="50">
                        <a href="/" target="_blank" style="color: white; text-decoration: none;">MAIN</a>
                        <a style="color: white; text-decoration: none;">|</a>
                        <a href="/all_projects" style="color: white; text-decoration: none;">PROJECTS</a>
                        <a style="color: white; text-decoration: none;">|</a>
                        <a href="/all_tasks" style="color: white; text-decoration: none;">TASKS</a>
                    </td>
                </tr>
                </tbody></table>
        </td>
        <td bgcolor="black">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <table width="100%" height="100" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; margin-left: 40px;">
            <tbody>
            <tr>
                <td width="100%" style="overflow: hidden;">
                    <h1 style="color: black; border-bottom: none; margin: 0; padding: 0;">PROJECT</h1>
                </td>
            </tr>
            </tbody>
        </table>
    </tr>

    <tr>
        <table width="30%" cellspacing="0" cellpadding="5" border="0" style="border-collapse: collapse; margin-left: 40px;">
            <tbody>
            <tr>
                <td>
                    NAME
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <input type="text" readonly value="${project.name}">
                </td>
            </tr>
            <tr>
                <td>
                    DESCRIPTION
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <input type="text" readonly value="${project.description}">
                </td>
            </tr>
            <tr>
                <td>
                    DATE START
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <fmt:formatDate value="${project.dateStart}" type="date" pattern="dd.MM.yyyy" var="fmt"/>
                    <input type="text" readonly value="${fmt}">
                </td>
            </tr>
            <tr>
                <td>
                    DATE FINISH
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <fmt:formatDate value="${project.dateFinish}" type="date" pattern="dd.MM.yyyy" var="fmt"/>
                    <input type="text" readonly value="${fmt}">
                </td>
            </tr>
            <tr>
                <td>
                    STATUS
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <input type="text" readonly value="${project.status}">
                </td>
            </tr>
            </tbody>
        </table>
</tbody></table>

</body></html>
